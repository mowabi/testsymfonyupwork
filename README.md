# Test Symfony pour Upwork

J’ai vu l’annonce et j’ai décidé de faire le test.

## Instructions

Développer un service Symfony permettant d’interpréter un fichier YAML contenant une liste d’organisations
Le service doit être capable de :

- Afficher sous forme de tableau HTML, les nom et descriptions des organisations
- Modifier pour une organisation, le nom, la description, la liste des utilisateurs (dans le fichier YAML) + formulaire
- Supprimer une organisation (du fichier YAML)
- Ajouter une organisation (dans le fichier YAML) + formulaire

Le fichier Yaml est donnée **(Dans le dossier src/Data)**

Utilisation d’un framework front possible
Pas de base de donnée
Symfony 4 de préférence
Utilisation de GIT obligatoire

## Ce que j’ai fait

À l’étape actuelle, j’ai mis en place l’affichage des organisations avec un formulaire pour ajouter de nouvelles organisations. J’ai aussi mis en place une route pour afficher chaque organisation avec la liste de ses utilisateurs.

## Ce qu’il me reste à faire

La suppression des organisateurs et des utilisateurs. Je mets ça en place si vous voulez toujours que je le fasse.
