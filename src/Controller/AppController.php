<?php

namespace App\Controller;

use App\Entity\Organization;
use App\Entity\User;
use App\Form\OrganizationType;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;

class AppController extends AbstractController
{

    /**
     * @Route("/", name="app_home")
     */
    public function index(Request $request): Response
    {
        $organizations = Yaml::parseFile("../src/Data/organizations.yaml");

        $organization = new Organization;

        $organizationForm = $this->createForm(OrganizationType::class, $organization);
        $organizationForm->handleRequest($request);

        if($organizationForm->isSubmitted() && $organizationForm->isValid()) {
            $organization = $organization->toArray();
            $lastOrganization = end($organizations["organizations"]);
            $lastId = $lastOrganization["id"];
            $organization["id"] = (int)$lastId + 1;
            
            $organizations["organizations"][] = $organization;

            //dd($organizations);

            $yaml = Yaml::dump($organizations, 2, 4, Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK);

            //dd($yaml);

            file_put_contents('../src/Data/organizations.yaml', $yaml);

            return $this->redirectToRoute("app_home");
        }

        return $this->render('home.html.twig', [
            'controller_name' => 'HomeController',
            "organizations" => $organizations["organizations"],
            "organizationForm" => $organizationForm->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="single_organization")
     */
    public function singleOrganization(int $id, Request $request): Response
    {
        $organizations = Yaml::parseFile("../src/Data/organizations.yaml");

        $organization = null;
        
        foreach ($organizations["organizations"] as $key => $singleOrganization) {
            if($singleOrganization["id"] == $id) {
                $organizationKey = $key;
                $organization = $singleOrganization;
            }
        }
        
        if($organization === null) {
            return new Response("not found", 404);
        }

        $user = new User;

        $userForm = $this->createForm(UserType::class, $user);
        $userForm->handleRequest($request);

        if($userForm->isSubmitted() && $userForm->isValid()) {
            $user = $user->toArray();

            $organization = $organizations["organizations"][$organizationKey];
            if($organization["users"]) {
            $lastUser = end($organization["users"]);
            $lastId = $lastUser["id"];
            } else {
                $lastId = 0;
            }
            $user["id"] = (int)$lastId + 1;
            $organization["users"][] = $user;
            
            $organizations["organizations"][$organizationKey] = $organization;

            $yaml = Yaml::dump($organizations, 2, 4, Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK);

            file_put_contents('../src/Data/organizations.yaml', $yaml);

            return $this->redirectToRoute("single_organization", ["id" => $id]);
        }

        return $this->render('organization.html.twig', [
            "organization" => $organization,
            "userForm" => $userForm->createView()
        ]);
    }
}
